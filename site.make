core = 7.x
api = 2

; Daily Bulletin
projects[uw_ct_daily_bulletin][type] = "module"
projects[uw_ct_daily_bulletin][download][type] = "git"
projects[uw_ct_daily_bulletin][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_daily_bulletin.git"
projects[uw_ct_daily_bulletin][download][tag] = "7.x-2.16"
projects[uw_ct_daily_bulletin][subdir] = ""

; Views RSS
; Base: 7.x-2.0-rc4
; Patch c13c74ba: "Replace relative paths with absolute URLs" broken. https://www.drupal.org/node/2442465#comment-9681385
projects[views_rss][type] = "module"
projects[views_rss][download][type] = "git"
projects[views_rss][download][url] = "https://git.uwaterloo.ca/drupal-org/views_rss.git"
projects[views_rss][download][tag] = "7.x-2.0-rc4-uw_wcms1"
projects[views_rss][subdir] = ""
